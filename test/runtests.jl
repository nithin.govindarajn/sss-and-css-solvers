using Test
using CSSandSSSsolvers
using CSSandSSSsolvers.CaseStudies
using Random
using LinearAlgebra

greet()


@testset "SSS matrices sanity check" begin

 
    # Construct SSS matrix directly from generators
    N = 20
    n = [5, 5, 4, 4, 4, 4, 6, 7, 8, 6, 5, 5, 4, 4, 4, 4, 6, 7, 8, 6]
    Gpi = [2, 3, 2, 2, 2, 2, 2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 2, 3]
    Hpi = [4, 2, 2, 4, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 2, 2, 4]


    Di = [rand(n[i], n[i]) for i = 1:N]
    Ui = [rand(n[i], Hpi[i]) for i = 1:N-1]
    Wi = [rand(Hpi[i], Hpi[i+1]) for i = 1:N-2]
    for i = 1:N-2
        Wi[i] = Wi[i] / (2 * norm(Wi[i]))
    end
    Vi = [rand(n[i+1], Hpi[i]) for i = 1:N-1]
    Pi = [rand(n[i+1], Gpi[i]) for i = 1:N-1]
    Ri = [rand(Gpi[i+1], Gpi[i]) for i = 1:N-2]
    for i = 1:N-2
        Ri[i] = Ri[i] / (2 * norm(Ri[i]))
    end
    Qi = [rand(n[i], Gpi[i]) for i = 1:N-1]

    A = SSSmatrix(N, n, Gpi, Hpi, Di, Ui, Wi, Vi, Pi, Ri, Qi)
    

    # SSS matrix construction from dense matrix
    n = [5, 5, 5, 5, 5, 5, 5, 5]
    N = sum(n)
    A = rand(N,N)
    A_SSS = constructSSS(A, n)
    Adense = SSStoDense(A_SSS)

    @test Adense ≈ A


    # SSS multiply
    b = rand(sum(n))
    c_SSS = SSSmatrixvectormultiply(A_SSS, b)
    c = A * b

    @test c_SSS ≈ c              # Error  original vs SSS


    # SSS solve
    x_SSS = SSSsolve(A_SSS, b)
    x = A \ b

    @test x_SSS ≈ x              # Error SSS solver vs Dense solver

end;


@testset "CSS matrices sanity check" begin


    # construct random circular SSS matrix
    N = 20
    n = [5, 5, 4, 4, 4, 4, 6, 7, 8, 6, 5, 5, 4, 4, 4, 4, 6, 7, 8, 6]
    Gpi = [2, 3, 2, 2, 2, 2, 2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 2, 3]
    Hpi = [4, 2, 2, 4, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 2, 2, 4]


    Di = [rand(n[i], n[i]) for i = 1:N]
    Ui = [rand(n[i], Hpi[i]) for i = 1:N-1]
    Wi = [rand(Hpi[i], Hpi[i+1]) for i = 1:N-2]
    for i = 1:N-2
        Wi[i] = Wi[i] / (2 * norm(Wi[i]))
    end
    Vi = [rand(n[i+1], Hpi[i]) for i = 1:N-1]
    V0 = rand(n[N], Hpi[1])
    Pi = [rand(n[i+1], Gpi[i]) for i = 1:N-1]
    P0 = rand(n[N], Gpi[1])
    Ri = [rand(Gpi[i+1], Gpi[i]) for i = 1:N-2]
    for i = 1:N-2
        Ri[i] = Ri[i] / (2 * norm(Ri[i]))
    end
    Qi = [rand(n[i], Gpi[i]) for i = 1:N-1]


    Acirc = CSSmatrix(N, n, Gpi, Hpi, Di, Ui, Wi, Vi, V0, Pi, Ri, Qi, P0)


    # CSS matrix construction from dense matrix
    n = [5, 5, 5, 5, 5, 5, 5, 5]
    N = sum(n)
    A = rand(N,N)
    A_CSS = constructCSS(A, n)
    Adense = CSStoDense(A_CSS)

    @test Adense ≈ A


    # CSS multiply
    b = rand(sum(n))
    c_CSS = CSSmatrixvectormultiply(A_CSS, b)
    c = A * b

    @test c_CSS ≈ c              # Error  original vs SSS


    # CSS solve
    x_CSS = CSSsolve(A_CSS, b)
    x = A \ b

    @test x_CSS ≈ x              # Error CSS solver vs Dense solver

end;


@testset "overlapping hankel LRCP" begin

   
    # Case: tridiagonal + sqrt(n) corner perturbations
    A = CaseStudies.perturbedsemisepbandedmatrix(100)
    n = [10, 10, 10, 10, 10, 10, 10, 10, 10, 10]


    Xbottomleft, Xupperright = overlapping_hankel_lrc(A, n)

    Xbottomlefttrue = A[91:100, 1:10]
    Xupperrighttrue = A[1:10, 91:100]

    @test Xbottomlefttrue ≈ Xbottomleft     # expected vs result for lower left corner
    @test Xupperrighttrue ≈ Xupperright     # expected vs result for upper right corner

end;





