

# intialize seed for random number generator
Random.seed!(1234);


###########################################################################################################################
######  Sanity check 0:                                                                                           #########
######  a tridiagonal matrix!                                                                                     #########
###########################################################################################################################

N = 8;
n = 2 * ones(Int64, 4)

# The matrix and its inverse
A_dense = tridiagonalexample(N)
Ainv_dense = inv(A_dense)
# SSS construction
A_SSS = constructSSS(A_dense, n)
Ainv_SSS = constructSSS(Ainv_dense, n)
# Circular SSS construction
A_circSSS = constructCSS(A_dense, n)
Ainv_circSSS = constructCSS(Ainv_dense, n)

# Print results
println("#####  Sanity check 0 #####")
println(" ")
println("SSS: Hankelblock ranks of A")
print("lower: ");
println(A_SSS.Gpi);
print("upper: ");
println(A_SSS.Hpi);
println("SSS: Hankelblock ranks of Ainv")
print("lower: ");
println(Ainv_SSS.Gpi);
print("upper: ");
println(Ainv_SSS.Hpi);
println(" ")
println("Circular SSS: Hankelblock ranks of A")
print("lower: ");
println(A_circSSS.Gpi);
print("upper: ");
println(A_circSSS.Hpi);
println("Circular SSS: Hankelblock ranks of Ainv")
print("lower: ");
println(Ainv_circSSS.Gpi);
print("upper: ");
println(Ainv_circSSS.Hpi);
println(" ")

###########################################################################################################################
######  Sanity check 1:                                                                                           #########
######  discrete analog  of  x'' + 2x = f on [0,1] with periodic boundary conditions x(0) = x(1), x'(0) = x'(1)   #########
###########################################################################################################################

N = 8;
n = 2 * ones(Int64, 4)

# The matrix and its inverse
A_dense = sanity_check1(N)
Ainv_dense = inv(A_dense)
# SSS construction
A_SSS = constructSSS(A_dense, n)
Ainv_SSS = constructSSS(Ainv_dense, n)
# Circular SSS construction
A_circSSS = constructCSS(A_dense, n)
Ainv_circSSS = constructCSS(Ainv_dense, n)

# Print results
println("#####  Sanity check 1 #####")
println(" ")
println("SSS: Hankelblock ranks of A")
print("lower: ");
println(A_SSS.Gpi);
print("upper: ");
println(A_SSS.Hpi);
println("SSS: Hankelblock ranks of Ainv")
print("lower: ");
println(Ainv_SSS.Gpi);
print("upper: ");
println(Ainv_SSS.Hpi);
println(" ")
println("Circular SSS: Hankelblock ranks of A")
print("lower: ");
println(A_circSSS.Gpi);
print("upper: ");
println(A_circSSS.Hpi);
println("Circular SSS: Hankelblock ranks of Ainv")
print("lower: ");
println(Ainv_circSSS.Gpi);
print("upper: ");
println(Ainv_circSSS.Hpi);
println(" ")


###########################################################################################################################
######  Sanity check 2:                                                                                           #########
######  Tridiagonal matrix with square root of n perturbation at cornes                                           #########
###########################################################################################################################

N = 100;
n = 10 * ones(Int64, 10)

# The matrix and its inverse
A_dense = sanity_check2(N)
Ainv_dense = inv(A_dense)
# SSS construction
A_SSS = constructSSS(A_dense, n)
Ainv_SSS = constructSSS(Ainv_dense, n)
# Circular SSS construction
A_circSSS = constructCSS(A_dense, n)
Ainv_circSSS = constructCSS(Ainv_dense, n)

# Print results
println("#####  Sanity check 2 #####")
println(" ")
println("SSS: Hankelblock ranks of A")
print("lower: ");
println(A_SSS.Gpi);
print("upper: ");
println(A_SSS.Hpi);
println("SSS: Hankelblock ranks of Ainv")
print("lower: ");
println(Ainv_SSS.Gpi);
print("upper: ");
println(Ainv_SSS.Hpi);
println(" ")
println("Circular SSS: Hankelblock ranks of A")
print("lower: ");
println(A_circSSS.Gpi);
print("upper: ");
println(A_circSSS.Hpi);
println("Circular SSS: Hankelblock ranks of Ainv")
print("lower: ");
println(Ainv_circSSS.Gpi);
print("upper: ");
println(Ainv_circSSS.Hpi);
println(" ")
