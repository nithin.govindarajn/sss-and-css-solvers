# CSSmatrices

This repository contains Julia code to construct and solve Cycle Semi-Separable (CSS) matrices. This includes functions to convert a dense matrix into CSS form. An overlapping minimum rank completion problem is solved. The repository also contains code to construct and solve ordinary Sequentially Semi-Seperable (SSS) matrices.

## How to use this code


## Running tests

To run the tests, follow the following steps:
1. Clone this repository to your local workstation
2. Open Julia REPL inside root directory of this folder.
3. Type "]" to enter the package manager.
4. Type "test" and press enter 

## Related literature

For more on SSS matrices:

Chandrasekaran, S., Dewilde, P., Gu, M., Pals, T., Sun, X., van der Veen, A. J., & White, D. (2005). Some fast algorithms for sequentially semiseparable representations. SIAM Journal on Matrix Analysis and Applications, 27(2), 341-364.

For more on CSS (and the more general G-SS) matrices:

Chandrasekaran, S., Epperly, E., & Govindarajan, N. (2019). Graph-induced rank structures and their representations. arXiv preprint arXiv:1911.05858.

For more on the theory conerning the overlapping rank completion problem:

Epperly, E. N., Govindarajan, N., & Chandrasekaran, S. (2021). Minimal Rank Completions for Overlapping Blocks. Linear Algebra and its Applications.











