import Base.display
using LinearAlgebra

ABS_TOL = 1e-13;
DEFAULT_TOL = 1e-10;

# Vertically concatenate array1 and array2, padding array1 on the right
# by zeros if necessary
function stack_triangular(array1, array2)
    num_zero_columns = size(array2, 2) - size(array1, 2);
    return vcat(hcat(array1, zeros(size(array1,1),num_zero_columns)),array2);
end

function blkdiag(A,B)
    return vcat(hcat(A,zeros(size(A,1),size(B,2))),
                hcat(zeros(size(B,1),size(A,2)),B));

end

# Finds the indices of the nonzero and zero singular values
function indices_nz_sv(mysvd;tol=DEFAULT_TOL)

    if isempty(mysvd.S)
        return [], [];
    end

    # println("Singular Values:")
    # display(mysvd.S)

    check = (mysvd.S .>= tol*mysvd.S[1]) .& (mysvd.S .>= ABS_TOL);
    nonzero_indices = sort(findall(check));
    zero_indices = sort([findall(.~check);
            length(mysvd.S)+1:min(size(mysvd.U,2),size(mysvd.V,2))]);

    return nonzero_indices, zero_indices;

end

struct ColumnIntersection

    #=
    Compute bases PAB, PAbar, and PBbar for R(A)∩R(B) and complementary row
    bases QABA, QAbar, QABB, and QBbar such that A = [PAB PAbar]*[QABA;QAbar]
    and B = [PAB PBbar]*[QABB;QBbar]. Also produce nonsingular matrices such
    that A*S_A = [PAB PAbar 0] and B*S_B = [PAB PBbar 0].
    =#
    PAB
    PAbar
    PBbar
    QTABA
    QTABB
    QTAbar
    QTBbar
    S_A
    S_B

    function ColumnIntersection(PAB,PAbar,PBbar,QTABA,QTABB,QTAbar,QTBbar,
                                S_A,S_B)

        return new(PAB,PAbar,PBbar,QTABA,QTABB,QTAbar,QTBbar,S_A,S_B);

    end

    function ColumnIntersection(A,B;tol=DEFAULT_TOL)

        # Notation: idx_A indices of nonzero singular values
        #           idx_A_z indices of zero singular values
        A_svd = svd(A, full=true);
        idx_A, idx_A_z = indices_nz_sv(A_svd,tol=tol);

        B_svd = svd(B, full=true);
        idx_B, idx_B_z = indices_nz_sv(B_svd,tol=tol);

        PA = A_svd.U[:,idx_A];
        PB = B_svd.U[:,idx_B];

        # The diagonal entries of S are the sines of the principal angles.
        # The intersection of the subspaces corresponds to the principal angles
        # which are ≈0
        proj_onto_A = PA*PA';
        N = (Matrix(I,size(proj_onto_A)) - proj_onto_A)*PB;
        N_svd = svd(N,full=false);
        idx_N_z = findall(N_svd.S .< tol);

        # The diagonal entries of the S are the cosines of the principal angles
        # Since the cosines of angles are largest for the smallest angles, the
        # indices of the smallest sine values occur are N-indices of the largest
        # cosine values
        M = PA'*PB;
        M_svd = svd(M,full=true);
        idx_M = sort(length(N_svd.S)+ 1 .-idx_N_z);
        YA = M_svd.U[:,idx_M];
        YB = M_svd.V[:,idx_M];

        sz = size(A,2)-length(idx_A);
        S_A = A_svd.V *
                blkdiag(Diagonal(1.0 ./ A_svd.S[idx_A]), Matrix(I,(sz,sz))) *
                blkdiag(M_svd.U, Matrix(I,sz,sz));

        sz = size(B,2)-length(idx_B);
        S_B = B_svd.V *
                blkdiag(Diagonal(1.0 ./ B_svd.S[idx_B]), Matrix(I,(sz,sz))) *
                blkdiag(M_svd.V, Matrix(I,sz,sz));

        ASA = A*S_A;
        BSA = B*S_B;
        @assert(norm(ASA[:,idx_M]-BSA[:,idx_M]) <= tol*sqrt(norm(A)*norm(B)) ||
                    norm(A)*norm(B) <= ABS_TOL^2);
        PAB = ASA[:,idx_M];

        idx_Abar = length(idx_M)+1:length(idx_A);
        PAbar = ASA[:,idx_Abar];

        idx_Bbar = length(idx_M)+1:length(idx_B);
        PBbar = BSA[:,idx_Bbar];

        S_A_inv = inv(S_A);
        QTABA = S_A_inv[idx_M,:];
        QTAbar = S_A_inv[idx_Abar,:];

        S_B_inv = inv(S_B);
        QTABB = S_B_inv[idx_M,:];
        QTBbar = S_B_inv[idx_Bbar,:];

        # # Printing for debug
        # println("PAB:", PAB)
        # println("PAbar:", PAbar)
        # println("PBbar:", PBbar)
        # println("QTABA:", QTABA)
        # println("QTABB:", QTABB)
        # println("QTAbar:", QTAbar)
        # println("QTBbar:", QTBbar)
        # println("S_A:", S_A)
        # println("S_B:", S_B)
        # println("A*S_A", A*S_A)
        # println()

        # output = new(PAB,PAbar,PBbar,QTABA,QTABB,QTAbar,QTBbar,S_A,S_B);
        # check_colint(output,A,B,tol=tol)

        return new(PAB,PAbar,PBbar,QTABA,QTABB,QTAbar,QTBbar,S_A,S_B);

    end

end

function check_colint(colint::ColumnIntersection,A,B;tol=DEFAULT_TOL)
    # # For debggging
    # println("Column Intersection Checking")
    # display(A)
    # display([colint.PAB colint.PAbar])
    # display([colint.QTABA;colint.QTAbar])
    # display([colint.PAB colint.PAbar]*[colint.QTABA;colint.QTAbar])
    # println()

    @assert(norm([colint.PAB colint.PAbar]*[colint.QTABA;colint.QTAbar]-A)
                < 10.0*tol*norm(A)+ABS_TOL)
    @assert(norm([colint.PAB colint.PBbar]*[colint.QTABB;colint.QTBbar]-B)
                < 10.0*tol*norm(B)+ABS_TOL)
    some_zeros = zeros(size(A,1),size(A,2)-size(colint.PAB,2)-
                    size(colint.PAbar,2));
    @assert(norm(A*colint.S_A-[colint.PAB colint.PAbar some_zeros]) <
                10.0*tol*norm(A)+ABS_TOL)
    some_zeros = zeros(size(B,1),size(B,2)-size(colint.PAB,2)-
                    size(colint.PBbar,2));
    @assert(norm(B*colint.S_B-[colint.PAB colint.PBbar some_zeros]) <
                10.0*tol*norm(B)+ABS_TOL)

end

function display(colint::ColumnIntersection)
    println("PAB")
    display(colint.PAB)
    println("PAbar")
    display(colint.PAbar)
    println("PBbar")
    display(colint.PBbar)
    println("QTABA")
    display(colint.QTABA)
    println("QTABB")
    display(colint.QTABB)
    println("QTAbar")
    display(colint.QTAbar)
    println("QTBbar")
    display(colint.QTBbar)
end

# Compute the intersection QAB of the row spaces of A and B
# and complementary subspaces QAbar and QBbar. Also Compute
# nonsingular matrices S_A and S_B such that S_A*A=[QAB;QAbar;0]
# and S_B*B=[QAB;QBbar;0]
struct RowIntersection

    QTAB
    QTAbar
    QTBbar
    PABA
    PAbar
    PABB
    PBbar
    S_A
    S_B

    function RowIntersection(QTAB, QTAbar, QTBbar, PABA, PAbar, PABB, PBbar,
                                S_A, S_B)

        return new(QTAB, QTAbar, QTBbar, PABA, PAbar, PABB, PBbar, S_A, S_B);

    end

    function RowIntersection(A,B;tol=DEFAULT_TOL)

        colint = ColumnIntersection(A',B',tol=tol);
        QTAB = colint.PAB';
        QTAbar = colint.PAbar';
        QTBbar = colint.PBbar';
        S_A = colint.S_A';
        S_B = colint.S_B';
        PABA = colint.QTABA';
        PABB = colint.QTABB';
        PAbar = colint.QTAbar';
        PBbar = colint.QTBbar';

        return new(QTAB, QTAbar, QTBbar, PABA, PAbar, PABB, PBbar, S_A, S_B);

    end

end

function check_rowint(rowint::RowIntersection,A,B;tol=DEFAULT_TOL)

    @assert(norm([rowint.PABA rowint.PAbar]*[rowint.QTAB;rowint.QTAbar]-A)
                < 10.0*tol*norm(A)+ABS_TOL)
    @assert(norm([rowint.PABB rowint.PBbar]*[rowint.QTAB;rowint.QTBbar]-B)
                < 10.0*tol*norm(B)+ABS_TOL)
    some_zeros = zeros(size(A,1)-size(rowint.QTAB,1)-size(rowint.QTAbar,1),
                        size(A,2));
    @assert(norm(rowint.S_A*A-[rowint.QTAB;rowint.QTAbar;some_zeros]) <
                10.0*tol*norm(A)+ABS_TOL)
    some_zeros = zeros(size(B,1)-size(rowint.QTAB,1)-size(rowint.QTBbar,1),
                        size(B,2));
    @assert(norm(rowint.S_B*B-[rowint.QTAB;rowint.QTBbar;some_zeros]) <
                10.0*tol*norm(B)+ABS_TOL)

end

function display(rowint::RowIntersection)
    println("QTBC")
    display(rowint.QTAB)
    println("QTBbar")
    display(rowint.QTAbar)
    println("QTCbar")
    display(rowint.QTBbar)
    println("PBCB")
    display(rowint.PABA)
    println("PBCC")
    display(rowint.PABB)
    println("PBbar")
    display(rowint.PAbar)
    println("PCbar")
    display(rowint.PBbar)
end

# A = [1 1 1;1 1 1;1 0 1];
# B = [2 3;2 3;2 3];
# ColumnIntersection(A,B)

# A = zeros(1,1);
# B = ones(1,2);
# colint = ColumnIntersection(A,B);
# println(colint.PAB)

# colint = ColumnIntersection(Array(I,1,1),zeros(1,0));
# println(colint.PBbar)
