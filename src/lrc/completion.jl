import Base.display
include("intersect.jl")
using LinearAlgebra

# Block lower triangular low-rank completion problem
struct LRCompletion

    A
    B
    C
    rowint
    colint
    R

    function LRCompletion(A,B,C;tol=DEFAULT_TOL)

        colint = ColumnIntersection(A,B,tol=tol);
        rowint = RowIntersection(B,C,tol=tol);

        PBbar = rowint.PAbar;
        PCbar = rowint.PBbar;
        PBCB = rowint.PABA;
        PBCC = rowint.PABB;

        R = hcat(PBbar, PBCB)\B/vcat(colint.QTABB,colint.QTBbar);

        return new(A,B,C,rowint,colint,R);

    end

    function LRCompletion(A,B,C,rowint,colint,R)

        return new(A,B,C,rowint,colint,R);

    end

end

function check_lrcompletion(lrc::LRCompletion;tol=DEFAULT_TOL)

    check_colint(lrc.colint, lrc.A, lrc.B, tol=tol);
    check_rowint(lrc.rowint, lrc.B, lrc.C, tol=tol);
    @assert(norm([get_PBbarB(lrc) get_PBCB(lrc)]*get_R(lrc)*
            [get_QTABB(lrc);get_QTBbarB(lrc)]-lrc.B) < 10.0*tol*norm(lrc.B)+
            ABS_TOL);

end

function get_PCbar(lrc::LRCompletion)
    return lrc.rowint.PBbar;
end

function get_QTAbar(lrc::LRCompletion)
    return lrc.colint.QTAbar;
end

function get_QTBbarB(lrc::LRCompletion)
    return lrc.colint.QTBbar;
end

function get_PBCC(lrc::LRCompletion)
    return lrc.rowint.PABB;
end

function get_QTABA(lrc::LRCompletion)
    return lrc.colint.QTABA;
end

function get_PBbar(lrc::LRCompletion)
    return lrc.rowint.PAbar;
end

function get_QTABB(lrc::LRCompletion)
    return lrc.colint.QTABB;
end

function get_R(lrc::LRCompletion)
    return lrc.R;
end

function get_R1(lrc::LRCompletion)
    return lrc.R[1:size(get_PBbarB(lrc),2),1:size(get_QTABB(lrc),1)];
end

function get_R2(lrc::LRCompletion)
    return lrc.R[1:size(get_PBbarB(lrc),2),size(get_QTABB(lrc),1)+1:end];
end

function get_R3(lrc::LRCompletion)
    return lrc.R[size(get_PBbarB(lrc),2)+1:end,1:size(get_QTABB(lrc),1)];
end

function get_R4(lrc::LRCompletion)
    return lrc.R[size(get_PBbarB(lrc),2)+1:end,size(get_QTABB(lrc),1)+1:end];
end

function get_PAB(lrc::LRCompletion)
    return lrc.colint.PAB;
end

function get_PAbar(lrc::LRCompletion)
    return lrc.colint.PAbar;
end

function get_PBbar(lrc::LRCompletion)
    return lrc.colint.PBbar;
end

function get_QTBbar(lrc::LRCompletion)
    return lrc.rowint.QTAbar;
end

function get_QTBC(lrc::LRCompletion)
    return lrc.rowint.QTAB;
end

function get_QTCbar(lrc::LRCompletion)
    return lrc.rowint.QTBbar;
end

function get_PBbarB(lrc::LRCompletion)
    return lrc.rowint.PAbar;
end

function get_PBCB(lrc::LRCompletion)
    return lrc.rowint.PABA;
end

function particular_solution(lrc::LRCompletion)
    return get_PBCC(lrc)*get_R3(lrc)*get_QTABA(lrc);
end

struct PartiallySpecifiedLRCompletion
# The genral solution to a row rank completion problem is of the form
# F1*QTAbar + PBCB*R2*QTABA + PCbar*F2, where F1 and F2 are free. When solving
# overlapping low rank completion problems, F2 must be set to a particular
# value to satisfy earlier low rank completion problems.

    lrc::LRCompletion
    F2

    function PartiallySpecifiedLRCompletion(A,B,C;tol=DEFAULT_TOL)
        lrc = LRCompletion(A,B,C,tol=tol);
        F2 = zeros(size(get_PCbar(lrc),2),size(A,2));
        return new(lrc,F2);
    end

    function PartiallySpecifiedLRCompletion(lrc::LRCompletion,F2)
        return new(lrc,F2);
    end

end

function particular_solution(pslrc::PartiallySpecifiedLRCompletion)
    return particular_solution(pslrc.lrc) + get_PCbar(pslrc.lrc)*pslrc.F2;
end

function display(pslrc::PartiallySpecifiedLRCompletion)
    println("Low Rank Completion Problem: QTAbar*F1 + P")
    println("QTAbar")
    display(get_QTAbar(pslrc.lrc))
    println("P")
    display(particular_solution(pslrc))
    println("---")

    println("A")
    display(pslrc.lrc.A)
    println("B")
    display(pslrc.lrc.B)
    println("C")
    display(pslrc.lrc.C)
    println("Column Intersection")
    display(pslrc.lrc.colint)
    println("Row Intersection")
    display(pslrc.lrc.rowint)
    println("R3")
    display(get_R3(pslrc.lrc))
    println("F2")
    display(pslrc.F2)
    println("---")
    println()
end

function removal_of_rows(pslrc::PartiallySpecifiedLRCompletion,num_rows;
                            tol=DEFAULT_TOL)

    PEF = get_PAB(pslrc.lrc);
    PEF_lower = PEF[1+num_rows:end,:];

    PEF_lower_svd = svd(PEF_lower, full=true);
    idx_PEF_lower, idx_PEF_lower_z = indices_nz_sv(PEF_lower_svd, tol=tol);
    PAB1 = PEF_lower_svd.U[:,idx_PEF_lower];

    # PEF*S0 = [PEF1 PEF2;PAB1 0]
    S0 = PEF_lower_svd.V * Diagonal(vec(1.0 ./ [PEF_lower_svd.S[idx_PEF_lower];
            ones(length(idx_PEF_lower_z),1)]));
    PEF12 = (PEF*S0)[1:num_rows,:];
    PEF1 = PEF12[:,1:size(PAB1,2)];
    PEF2 = PEF12[:,size(PAB1,2)+1:end];

    PEbar = get_PAbar(pslrc.lrc);
    PEbar_lower = PEbar[1+num_rows:end,:];

    B = pslrc.lrc.B[1+num_rows:end,:];
    PEbar_lower_colint = ColumnIntersection(PEbar_lower, B,tol=tol);
    PAbar = PEbar_lower_colint.PAbar;
    PAB2p = PEbar_lower_colint.PAB;

    PAB2p_colint = ColumnIntersection(PAB2p, PAB1, tol=tol);
    PAB2 = PAB2p_colint.PAbar;

    S1 = PEbar_lower_colint.S_A;
    S2 = PAB2p_colint.S_A;
    m = size(S1,1) - size(S2,1);
    S4 = S1*blkdiag(S2,Array(I,m,m));

    m1 = size(PAB2p_colint.PAB,2);
    m2 = size(PAB2,2);
    m3 = size(PAB2p,2) - m1 - m2;
    m4 = size(PAbar,2);
    m5 = size(PEbar_lower,2) - m1 - m2 - m3 - m4;
    S4 = S4[:,[1+m1:m1+m2;m1+m2+m3+1:m1+m2+m3+m4;1:m1;m1+m2+1:m1+m2+m3;
            m1+m2+m3+m4+1:m1+m2+m3+m4+m5]];

    m1 = size(PAB1,2);
    m2 = size(PEF_lower,2)-m1;
    S3 = PAB2p_colint.S_B;
    S5 = S0*blkdiag(S3,Array(I,m2,m2));

    S5S6 = zeros(size(S5,1),size(S4,2));
    m1 = size(PAbar,2);
    m2 = size(PAB2,2);
    m3 = size(PAB2p_colint.PAB,2);
    m4 = size(PAB2p_colint.PAbar,2);
    S5S6[:,m1+m2+1:m1+m2+m3] = S5[:,1+m4:m4+m3];

    Φ = stack_triangular(S4,hcat(-S5S6,S0));
    m1 = size(PAB2,2);
    m2 = size(PAbar,2);
    m3 = size(PEbar_lower,2)-m1-m2;
    m4 = size(PAB1,2);
    m5 = size(PEF_lower,2)-m4;
    Φ = Φ[:,[1:m1;m1+m2+m3+1:m1+m2+m3+m4;m1+1:m1+m2;
            m1+m2+m3+m4+1:m1+m2+m3+m4+m5;m1+m2+1:m1+m2+m3]];

    PFbar = get_PBbar(pslrc.lrc);
    PFbar_lower = PFbar[1+num_rows:end,:];

    A = pslrc.lrc.A[1+num_rows:end,:];
    PFbar_lower_colint = ColumnIntersection(PFbar_lower, A,tol=tol);
    PBbar = PFbar_lower_colint.PAbar;
    PAB2pp = PFbar_lower_colint.PAB;

    PAB2pp_colint = ColumnIntersection(PAB2pp, PAB1, tol=tol);
    PAB2d = PAB2pp_colint.PAbar;

    S7 = PFbar_lower_colint.S_A;
    S8 = PAB2pp_colint.S_A;
    m = size(S7,1) - size(S8,1);
    S10 = S7*blkdiag(S8,Array(I,m,m));

    m1 = size(PAB2pp_colint.PAB,2);
    m2 = size(PAB2d,2);
    m3 = size(PAB2pp,2) - m1 - m2;
    m4 = size(PBbar,2);
    m5 = size(PFbar_lower,2) - m1 - m2 - m3 - m4;
    S10 = S10[:,[1+m1:m1+m2;m1+m2+m3+1:m1+m2+m3+m4;1:m1;m1+m2+1:m1+m2+m3;
            m1+m2+m3+m4+1:m1+m2+m3+m4+m5]];

    m1 = size(PAB1,2);
    m2 = size(PEF_lower,2)-m1;
    S9 = PAB2pp_colint.S_B;
    S11 = S0*blkdiag(S9,Array(I,m2,m2));

    S11S12 = zeros(size(S0,1),size(S10,2));
    m1 = size(PBbar,2);
    m2 = size(PAB2d,2);
    m3 = size(PAB2pp_colint.PAB,2);
    S11S12[:,m1+m2+1:m1+m2+m3] = S11[:,1:m3];

    Ψ = stack_triangular(S10,hcat(-S11S12,S0));
    m1 = size(PAB2d,2);
    m2 = size(PBbar,2);
    m3 = size(PFbar_lower,2)-m1-m2;
    m4 = size(PAB1,2);
    m5 = size(PEF_lower,2)-m4;
    Ψ = Ψ[:,[1:m1;m1+m2+m3+1:m1+m2+m3+m4;m1+1:m1+m2;
            m1+m2+m3+m4+1:m1+m2+m3+m4+m5;m1+m2+1:m1+m2+m3]];

    m = size(PEF,2) + size(PFbar,2) - size([PAB2 PAB1],2);
    Ψ = Ψ*blkdiag([PAB2d PAB1]\[PAB2 PAB1],Array(I,m,m));

    PAB = [PAB2 PAB1];

    m1 = size(PEbar_lower,2);
    m2 = size(PEF_lower,2);
    Φ = Φ[[m1+1:m1+m2;1:m1],:];

    m1 = size(PFbar_lower,2);
    m2 = size(PEF_lower,2);
    Ψ = Ψ[[m1+1:m1+m2;1:m1],:];

    # # For debugging
    # println("Removal of Rows -- Column Intersection")
    # display([PEbar_lower PEF_lower] * Φ)
    # display(PAB2)
    # display(PAB1)
    # display(PAbar)
    # println()

    QTA = Φ\[get_QTABA(pslrc.lrc);get_QTAbar(pslrc.lrc)];

    m1 = size(PAB,2);
    m2 = size(PAbar,2);
    m3 = size(PEF_lower,2)-size(PAB1,2);
    QTAbar = QTA[m1+1:m1+m2,:];
    QTABA = QTA[1:m1,:];
    QTEFE2 = QTA[m1+m2+1:m1+m2+m3,:];

    QTB = Ψ\[get_QTABB(pslrc.lrc);get_QTBbarB(pslrc.lrc)];

    m1 = size(PAB,2);
    m2 = size(PBbar,2);
    QTBbar = QTB[m1+1:m1+m2,:];
    QTBbarB = QTBbar;
    QTABB = QTB[1:m1,:];

    m = size(A,2) - size([PEF_lower PEbar_lower],2);
    S_A = pslrc.lrc.colint.S_A*blkdiag(Φ,Array(I,m,m));
    m = size(B,2) - size([PEF_lower PFbar_lower],2);
    S_B = pslrc.lrc.colint.S_B*blkdiag(Ψ,Array(I,m,m));

    new_colint = ColumnIntersection(PAB,PAbar,PBbar,QTABA,QTABB,QTAbar,QTBbar,
                                    S_A,S_B);

    C = pslrc.lrc.C;
    B_rowint = RowIntersection(B,C,tol=tol);
    QTBC = B_rowint.QTAB;
    QTBbar = B_rowint.QTAbar;

    QTFC = get_QTBC(pslrc.lrc);
    QTFC_rowint = RowIntersection(QTFC,QTBC,tol=tol);

    QTFC2 = QTFC_rowint.QTAbar;
    @assert(size(QTFC,1) == size(QTBC,1)+size(QTFC2,1))
    T3 = [QTBC;QTFC2]/QTFC;

    F = pslrc.lrc.B[1:num_rows,:];
    QTFbar = get_QTBbar(pslrc.lrc);
    FB_rowint = RowIntersection([F;B],[QTFC;QTBbar],tol=tol);

    QTFbar2 = FB_rowint.QTAbar;
    @assert(size(QTFbar,1) == size(QTBbar,1)+size(QTFbar2,1))

    Ω = [QTBbar;QTFbar2;QTBC;QTFC2]/[QTFbar;QTFC];

    QTCbar = [QTFC2;get_QTCbar(pslrc.lrc)];

    PFCC = get_PBCC(pslrc.lrc);
    PFCCT3inv = PFCC/T3;
    PBCC = PFCCT3inv[:,1:size(QTBC,1)];
    PCbar = [PFCCT3inv[:,size(QTBC,1)+1:end] get_PCbar(pslrc.lrc)];

    PFbarF = get_PBbarB(pslrc.lrc);
    PFCF = get_PBCB(pslrc.lrc);

    PB = B/[QTBbar;QTBC];
    PBbarB = PB[:,1:size(QTBbar,1)];
    PBCB = PB[:,1+size(QTBbar,1):end];

    m = size(F,1)+size(B,1)-size(Ω,1);
    n1 = size(QTFbar,1);
    n2 = n1 + size(QTFC,1);
    Ωperm = Ω[[n1+1:n2;1:n1],[n1+1:n2;1:n1]];
    G = blkdiag(Ωperm,Array(I,m,m))*pslrc.lrc.rowint.S_A;

    n1 = size(QTBC,1);
    n2 = size(QTFC2,1)+n1;
    n3 = size(QTBbar,1)+n2;
    n4 = size(QTFbar2,1)+n3;
    n5 = size(F,1)+size(B,1);
    G = G[[1:n1;n2+1:n3;n4+1:n5;n1+1:n2;n3+1:n4],:];
    S_B = G[1:size(B,1),1+size(F,1):end];

    m = size(C,1)-size(T3,1);
    S_C = blkdiag(T3,Array(I,m,m))*pslrc.lrc.rowint.S_B;

    new_rowint = RowIntersection(QTBC, QTBbar, QTCbar, PBCB, PBbarB, PBCC,
                                PCbar, S_B, S_C);

    Ψterm = Ψ[:,1:size(QTABB,1)+size(QTBbarB,1)];
    Ωterm = Ω[[1:size(PBbarB,2);
                size(QTFbar,1)+1:size(QTFbar,1)+size(PBCB,2)],:];
    new_R = Ωterm * get_R(pslrc.lrc) * Ψterm;

    new_lrc = LRCompletion(A,B,C,new_rowint,new_colint,new_R);
    check_lrcompletion(new_lrc)

    Y2 = T3[end+1-size(QTFC2,1):end,:];
    old_R3 = get_R3(pslrc.lrc);
    QTAB1A = QTABA[1:size(PAB1,2),:];

    new_F2 = [Y2*old_R3*S0*[QTAB1A;QTEFE2];pslrc.F2];
    return PartiallySpecifiedLRCompletion(new_lrc,new_F2)

end

function special_row_basis_construction(VT1,VT2,S,QT11,QT12,QT22,QT31,VT31,VT32;
                            tol=DEFAULT_TOL)

    #=
    Take as input matrices B, G and a nonsingular matrix S such that
        S*[B G] = [QT11 QT12;0 QT22;VT31,VT32]
    where [QT11 QT12;0 QT22] has full row rank and [QT11;QT31] is a row basis
    for B. Produce a matrix T such that
        T*[B G] = [QT11 QT12;0 QT22;QT31 QT32;0 QT42]
    =#

    QT11_rowint = RowIntersection(QT11,VT31,tol=tol);

    S4 = QT11_rowint.S_B;
    S5 = QT11_rowint.S_A;
    S6 = zeros(size(S4,1),size(S5,2));
    S6[1:size(QT11_rowint.QTAB,1),:] = S5[1:size(QT11_rowint.QTAB,1),:];

    m1 = size(QT11_rowint.QTAB,1);
    m2 = size(QT11_rowint.QTBbar,1);
    m3 = size(VT31,1) - m1 - m2;
    πS4 = S4[[m1+1:m1+m2;1:m1;m1+m2+1:m1+m2+m3],:];
    πS6 = S6[[m1+1:m1+m2;1:m1;m1+m2+1:m1+m2+m3],:];

    n1 = size(QT11,1);
    n2 = n1 + size(QT22,1);
    n3 = n2 + size(VT31,1);
    S7 = deepcopy(S);
    S7[n2+1:n3,:] = -πS6*S7[1:n1,:] + πS4*S7[n2+1:n3,:];

    YT2 = QT11_rowint.QTBbar;
    T_lower = QT31 / [QT11;YT2];
    T21 = T_lower[:,1:size(QT11,1)];
    T22 = T_lower[:,1+size(QT11,1):end];

    n1 = size(QT11,1);
    n2 = n1 + size(QT22,1);
    n3 = n2 + size(QT31,1);
    n4 = n2 + size(VT31,1);
    K = deepcopy(S7);
    K[n2+1:n3,:] = T21*K[1:n1,:] + T22*K[n2+1:n3,:];

    KV = K*[VT1 VT2];
    QT32 = KV[n2+1:n3,1+size(VT1,2):end];
    QT42 = KV[n3+1:n4,1+size(VT1,2):end];

    return QT32, QT42, K;

end

function addition_of_columns(pslrc::PartiallySpecifiedLRCompletion,columns;
                            tol=DEFAULT_TOL)

    A = pslrc.lrc.A;
    B = pslrc.lrc.B;
    C = pslrc.lrc.C;
    G = columns[1:size(A,1),:];
    H = columns[size(A,1)+1:size(A,1)+size(C,1),:];

    @assert(size(columns,1) == size(A,1)+size(C,1))

    BG = [B G];
    CH = [C H];

    BG_rowint = RowIntersection(BG,CH,tol=tol);
    S1 = BG_rowint.S_A;
    S2 = BG_rowint.S_B;

    QTGH_BC = BG_rowint.QTAB[:,1:size(B,2)];
    QTGH_BC_svd = svd(QTGH_BC,full=true);
    idx, idz_z = indices_nz_sv(QTGH_BC_svd,tol=tol);
    S3 = QTGH_BC_svd.U';
    QTBC1 = Diagonal(QTGH_BC_svd.S[idx])*QTGH_BC_svd.Vt[idx,:];
    QTGH = S3*BG_rowint.QTAB;

    QTBC = get_QTBC(pslrc.lrc);
    QTBC_rowint = RowIntersection(QTBC, QTBC1, tol=tol);
    S4 = QTBC_rowint.S_A;
    QTBC2 = QTBC_rowint.QTAbar;
    @assert(size(QTBC_rowint.QTAB,1)+size(QTBC_rowint.QTAbar,1) == size(QTBC,1))
    m = size(QTBC2,1);
    S5 = blkdiag(QTBC1/QTBC_rowint.QTAB, Array(I,m,m))*S4;

    QTBbar = get_QTBbar(pslrc.lrc);
    QTGbarB = BG_rowint.QTAbar[:,1:size(B,2)];
    QTGbarG = BG_rowint.QTAbar[:,1+size(B,2):end];

    VT1 = [QTBC1;zeros(size(QTGH_BC,1)-size(QTBC1,1),size(B,2));QTGbarB];
    VT2 = [QTGH[:,1+size(B,2):end];QTGbarG];
    m = size(QTGbarG,1);
    S = blkdiag(S3,Array(I,m,m));
    QT11 = QTBC1;
    QT12 = QTGH[1:size(QTBC1,1),1+size(B,2):end];
    QT22 = QTGH[1+size(QTBC1,1):end,1+size(B,2):end];
    QT31 = [QTBC2;QTBbar];
    VT31 = QTGbarB;
    VT32 = QTGbarG;

    _, _, S6 = special_row_basis_construction(VT1,VT2,S,QT11,QT12,QT22,
                        QT31,VT31,VT32,tol=tol);

    m = size(B,1)-size(S6,1)
    S7 = blkdiag(S6,Array(I,m,m)) * S1;

    QTCbar = get_QTCbar(pslrc.lrc);
    QTHbarC = BG_rowint.QTBbar[:,1:size(B,2)];
    QTHbarH = BG_rowint.QTBbar[:,1+size(B,2):end];

    VT1 = [QTBC1;zeros(size(QTGH_BC,1)-size(QTBC1,1),size(B,2));QTHbarC];
    VT2 = [QTGH[:,1+size(B,2):end];QTHbarH];
    m = size(QTHbarH,1);
    S = blkdiag(S3,Array(I,m,m));
    QT11 = QTBC1;
    QT12 = QTGH[1:size(QTBC1,1),1+size(B,2):end];
    QT22 = QTGH[1+size(QTBC1,1):end,1+size(B,2):end];
    QT31 = [QTBC2;QTCbar];
    VT31 = QTHbarC;
    VT32 = QTHbarH;

    _, _, S8 = special_row_basis_construction(VT1,VT2,S,QT11,QT12,QT22,
                        QT31,VT31,VT32,tol=tol);

    m = size(C,1)-size(S8,1)
    S9 = blkdiag(S8,Array(I,m,m)) * S2;

    S7BG = S7*BG;
    QTGbar = S7BG[1+size(QTGH,1):size(QTGH,1)+size(QTGbarB,1),:];
    S9CH = S9*CH;
    QTHbar = S9CH[1+size(QTGH,1):size(QTGH,1)+size(QTHbarC,1),:];

    S_G = S7;
    S_G_inv = inv(1.0*S_G);

    PGHG = S_G_inv[:,1:size(QTGH,1)];
    PGbarG = S_G_inv[:,size(QTGH,1)+1:size(QTGH,1)+size(QTGbar,1)];

    S_H = S9;
    S_H_inv = inv(S_H);

    PGHH = S_H_inv[:,1:size(QTGH,1)];
    PHbar = S_H_inv[:,size(QTGH,1)+1:size(QTGH,1)+size(QTHbar,1)];

    new_rowint = RowIntersection(QTGH, QTGbar, QTHbar, PGHG, PGbarG, PGHH,
                                PHbar, S_G, S_H);

    G_colint = ColumnIntersection(G,A,tol=tol);
    PGcapA = G_colint.PAB;
    PGcapAbar = G_colint.PAbar;
    T1 = G_colint.S_A;

    PAbar = get_PAbar(pslrc.lrc);
    PAB = get_PAB(pslrc.lrc);
    A_colint = ColumnIntersection(A,[PAB PGcapA],tol=tol);
    PAbarbullet = A_colint.PAbar;
    T2 = A_colint.S_A;
    @assert(size(PGcapA,2)+size(PAbarbullet,2) == size(PAbar,2));

    PAB = get_PAB(pslrc.lrc);
    T3 = [PAbar PAB]\[PAbarbullet PGcapA PAB];

    PAG = [PAB PGcapA];
    PBbar = get_PBbar(pslrc.lrc);
    PGbar = [PBbar PGcapAbar];

    T3invQTAbarQTABA = T3\[get_QTAbar(pslrc.lrc);get_QTABA(pslrc.lrc)];
    n1 = size(PAbarbullet,2);
    n2 = n1 + size(PGcapA,2);
    QTAbarbullet = T3invQTAbarQTABA[1:n1,:];
    QTGcapAA = T3invQTAbarQTABA[1+n1:n2,:];
    QTAGA = [get_QTABA(pslrc.lrc);QTGcapAA];

    T1inv = inv(T1);
    n1 = size(PGcapA,2);
    n2 = n1 + size(PGcapAbar,2);
    QTGcapAG = T1inv[1:n1,:];
    QTGcapAbar = T1inv[n1+1:n2,:];
    QTAGG = blkdiag(get_QTABB(pslrc.lrc),QTGcapAG);
    QTGbarG = blkdiag(get_QTBbarB(pslrc.lrc),QTGcapAbar);


    T3_permuted = T3;
    m1 = size(PAbarbullet,2);
    m2 = size(PGcapA,2);
    m3 = size(PAB,2);
    T3_permuted = T3_permuted[[m1+m2+1:m1+m2+m3;1:m1+m2],
                                [m1+m2+1:m1+m2+m3 ; m1+1:m1+m2 ; 1:m1]];
    m = size(A,2) - m1 - m2 - m3;
    S_A = pslrc.lrc.colint.S_A*blkdiag(T3_permuted,Array(I,m,m));

    S_B = blkdiag(pslrc.lrc.colint.S_B,T1);
    n1 = size(PAB,2);
    n2 = n1+size(PBbar,2);
    n3 = size(B,2);
    n4 = n3+size(PGcapA,2);
    n5 = n4+size(PGcapAbar,2);
    n6 = n3+size(G,2);
    S_G = S_B[:,[1:n1;n3+1:n4;n1+1:n2;n4+1:n5;n2+1:n3;n5+1:n6]];

    new_colint = ColumnIntersection(PAG,PAbarbullet,PGbar,
                                    QTAGA,QTAGG,QTAbarbullet,QTGbarG,
                                    S_A,S_G);

    extra_R_columns = [PGbarG PGHG]\G/[QTGcapAG;QTGcapAbar];

    S5R3 = S5*get_R3(pslrc.lrc);
    S5R4 = S5*get_R4(pslrc.lrc);

    m1 = size(QTBC2,1);
    m2 = size(QTBbar,1);
    m3 = size(QTGbar,1) - m1 - m2;
    m4 = size(QTBC1,1);
    m5 = size(QTGH,1) - m4;
    Rp11 = S5R3[m4+1:end,:];
    Rp13 = S5R4[m4+1:end,:];
    Rp21 = get_R1(pslrc.lrc);
    Rp23 = get_R2(pslrc.lrc);
    Rp31 = zeros(m3,size(Rp11,2));
    Rp33 = zeros(m3,size(Rp13,2));
    Rp41 = S5R3[1:m4,:];
    Rp43 = S5R4[1:m4,:];
    Rp51 = zeros(m5,size(Rp11,2));
    Rp53 = zeros(m5,size(Rp13,2));

    Rp_col1 = [Rp11;Rp21;Rp31;Rp41;Rp51];
    Rp_col2 = extra_R_columns[:,1:size(PGcapA,2)];
    Rp_col3 = [Rp13;Rp23;Rp33;Rp43;Rp53];
    Rp_col4 = extra_R_columns[:,size(PGcapA,2)+1:end];

    new_R = [Rp_col1 Rp_col2 Rp_col3 Rp_col4];

    new_lrc = LRCompletion(A,BG,CH,new_rowint,new_colint,new_R);
    check_lrcompletion(new_lrc,tol=tol);

    m = size(QTHbar,1) - size(QTCbar,1) - size(QTBC2,1);
    new_F2 = [Rp11*get_QTABA(pslrc.lrc);pslrc.F2;zeros(m,size(pslrc.F2,2))];

    return PartiallySpecifiedLRCompletion(new_lrc, new_F2);

end

# A = Array{Float64}(I,2,2);
# B = [0.0 0.0;1.0 1.0];
# # B = [1.0 1.0;0.0 0.0];
# C = [1.0 1.0;1 0];
#
# pslrc = PartiallySpecifiedLRCompletion(A,B,C);
# println(particular_solution(pslrc));
# pslrc = removal_of_rows(pslrc,1);
# println(particular_solution(pslrc));
# pslrc = addition_of_columns(pslrc,[0;2;3]);
# println(particular_solution(pslrc));
