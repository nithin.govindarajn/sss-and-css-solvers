struct TriangularArray
    array
    row_ends
    column_ends
    num_blocks
    function TriangularArray(array, row_ends, column_ends)
        @assert(length(column_ends) == length(row_ends))
        @assert(row_ends[end] == size(array, 1))
        @assert(column_ends[end] == size(array, 2))
        new(array, row_ends, column_ends, length(column_ends) - 1)
    end
end

function Base.string(array::TriangularArray)
    output = ""
    for i = 1:array.num_blocks
        for row = array.row_ends[i]+1:array.row_ends[i+1]
            for column = 1:array.column_ends[i+1]
                output *= string(array[row, column]) * "\t"
            end
            output *= "\n"
        end
    end
    return output[1:end-1]
end

function Base.print(array::TriangularArray)
    print(string(array))
end

function Base.println(array::TriangularArray)
    println(string(array))
end

function Base.size(array::TriangularArray)
    return size(array.array)
end

function Base.getindex(array::TriangularArray, i, j)
    return getindex(array.array, i, j)
end

function get_rowblock(array::TriangularArray, i)
    @assert(1 <= i <= array.num_blocks)
    return array[array.row_ends[i]+1:array.row_ends[i+1],
        1:array.column_ends[i+1]]
end

function get_columnblock(array::TriangularArray, i)
    @assert(1 <= i <= array.num_blocks)
    return array[array.row_ends[i]+1:array.row_ends[end],
        array.column_ends[i]+1:array.column_ends[i+1]]
end

function get_subblock(array::TriangularArray, i, j)
    @assert(1 <= j <= i <= array.num_blocks)
    return array[array.row_ends[i]+1:array.row_ends[i+1],
        array.column_ends[j]+1:array.column_ends[j+1]]
end

function flip(array::TriangularArray)
    new_array = zeros(size(array.array'))
    row_ends = Array{Int64}(undef, size(array.row_ends))
    column_ends = Array{Int64}(undef, size(array.column_ends))

    num_rows = size(new_array, 1)
    row_ends[end] = num_rows
    column_ends[1] = 0

    num_blocks = array.num_blocks

    for i = 1:num_blocks
        column_ends[i+1] = array.row_ends[num_blocks+2-i] -
                           array.row_ends[num_blocks+1-i] +
                           column_ends[i]
        row_ends[i] = num_rows - array.column_ends[num_blocks+2-i]
    end

    for i = 1:array.num_blocks
        for j = 1:i
            new_array[row_ends[i]+1:row_ends[i+1],
                column_ends[j]+1:column_ends[j+1]] =
                transpose(get_subblock(array, num_blocks - j + 1, num_blocks - i + 1))
        end
    end

    return TriangularArray(new_array, row_ends, column_ends)
end
