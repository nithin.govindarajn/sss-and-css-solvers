include("intersect.jl")
include("triangular_array.jl")

function eliminate_up(E,F,A,B;tol=DEFAULT_TOL)

    # Perform block Gaussian elimination to reduce down to
    # [G [0;QTFbar;0]    ]
    # [A B              C]
    FB_intersection = RowIntersection(F,B,tol=tol);
    S_F = FB_intersection.S_A;
    S_B = FB_intersection.S_B;
    m1 = size(FB_intersection.QTAB,1);
    S_B_aug = vcat(S_B[1:m1,:],zeros(size(S_F,1)-m1,size(S_B,2)));
    G = S_F*E - S_B_aug*A;

    QTFbar = FB_intersection.QTAbar;
    m2 = size(FB_intersection.QTAbar,1);
    G13 = G[[1:m1;m1+m2+1:end],:];
    _, G13 = rank_factorization(G13,tol=tol);
    new_rows = stack_triangular(G13,hcat(G[m1+1:m1+m2,:],QTFbar));
    # @assert(size(new_rows,1)>0);

    return new_rows

end

function horizontal_compress_triangular_array(array::TriangularArray;
    tol=DEFAULT_TOL)

    if array.num_blocks <= 1
        return array;
    end

    num_blocks = array.num_blocks;
    num_rows = zeros(num_blocks+1,1);

    new_array = get_rowblock(array, num_blocks);
    num_rows[end] = size(new_array,1);
    num_rows_bottom_block = size(new_array,1);

    _, new_rows = rank_factorization(get_rowblock(array,
        num_blocks-1),tol=tol);
    new_array = stack_triangular(new_rows, new_array);
    num_rows[end-1] = size(new_rows,1);

    #=
    We have now compressed the (i+1)st Hankel block and wish to compress the
    ith:
    i:         [ E F   ]
    i+1 to n:  [ A B C ]
    =#


    second_column_index = array.column_ends[2]+1;
    for i = num_blocks-2:-1:1
        rowblock = get_rowblock(array, i);
        _, rowblock = rank_factorization(rowblock,tol=tol);
        E = rowblock[:,1:second_column_index-1];
        F = rowblock[:,second_column_index:end];
        A = new_array[1:end-num_rows_bottom_block,1:second_column_index-1];
        B = new_array[1:end-num_rows_bottom_block,
                        second_column_index:size(rowblock,2)];

        # Perform block Gaussian elimination to reduce down to
        # [G [0;QTFbar;0]    ]
        # [A B              C]
        new_rows = eliminate_up(E,F,A,B);

        new_array = stack_triangular(new_rows, new_array);
        num_rows[i+1] = size(new_rows,1);
    end

    row_ends = zeros(size(num_rows));
    for i=2:num_blocks+1
        row_ends[i] = row_ends[i-1] + num_rows[i];
    end

    row_ends = round.(Int,row_ends);

    return TriangularArray(new_array, row_ends, array.column_ends);

end

function compress_triangular_array(array::TriangularArray; tol=DEFAULT_TOL)

    # Compress rows
    array = horizontal_compress_triangular_array(array, tol=tol);
    # Compress columns
    array = flip(horizontal_compress_triangular_array(flip(array),tol=tol));
    return array;

end


function rank_factorization(array; tol=DEFAULT_TOL)

    if size(array,1)*size(array,2) == 0
        return zeros(size(array,1),0), zeros(0,size(array,2));
    elseif size(array,1) == 1
        if norm(array) > ABS_TOL
            return 1.0, array;
        else
            return zeros(size(array,1),0), zeros(0,size(array,2));
        end
    elseif size(array,2) == 1
        if norm(array) > ABS_TOL
            return array, 1.0;
        else
            return zeros(size(array,1),0), zeros(0,size(array,2));
        end
    end

    mysvd = svd(array);
    indices = findall((mysvd.S .>= tol*mysvd.S[1]) .& (mysvd.S .>= ABS_TOL));
    return mysvd.U[:,indices]*Diagonal(mysvd.S[indices]), mysvd.Vt[indices,:];

end

# # Debugging example
# A = TriangularArray([1 0 0 0;2 2 0 0;3 4 4 0;4 4 4 4],0:4,0:4)
# println(A);
# # println(horizontal_compress_triangular_array(A));
# println(compress_triangular_array(A));
