
module OverlappingHankel

 
export overlapping_hankel_lrc

include("completion.jl")
include("compression.jl")



function overlapping_hankel_lrc(array, block_sizes; tol = DEFAULT_TOL,
    alg = "two-stage")

    row_ends = Array{Integer}([0; cumsum(block_sizes)])

    # # For Debugging
    # println("Lower triangular part")
    X_lower = overlapping_hankel_lrc_lower(array, row_ends, tol = tol,
        alg = "two-stage")

    # # For Debugging
    # println("Upper triangular part")
    X_upper = overlapping_hankel_lrc_lower(array', row_ends, tol = tol,
        alg = "two-stage")'

    return X_lower, X_upper

end

function overlapping_hankel_lrc_lower(array, row_ends; tol = DEFAULT_TOL,
    alg = "two-stage")

    @assert(row_ends[end] == size(array, 1))

    lower_array = array[row_ends[2]+1:end, 1:row_ends[end-1]]
    lower_row_ends = row_ends[2:end] .- row_ends[2]
    lower_col_ends = row_ends[1:end-1]

    lower_triangular_array = TriangularArray(lower_array, lower_row_ends,
        lower_col_ends)
    if alg == "two-stage"
        X_lower = overlapping_hankel_lrc_triangle_twostage(
            lower_triangular_array, tol = tol)
    elseif alg == "one-stage"
        X_lower = overlapping_hankel_lrc_triangle_onestage(
            lower_triangular_array, tol = tol)
    else
        throw(DomainError(alg, "This algorithm type is not supported"))
    end

    return X_lower

end

function overlapping_hankel_lrc_triangle_twostage(array::TriangularArray;
    tol = DEFAULT_TOL)

    array = compress_triangular_array(array, tol = tol)

    num_blocks = array.num_blocks

    if (num_blocks == 1)
        return zeros(size(get_subblock(array, num_blocks, 1)))
    end

    X_size = size(get_subblock(array, num_blocks, 1))
    X_rows = X_size[1]

    first_hankel_block = get_columnblock(array, 1)
    second_hankel_block = get_columnblock(array, 2)

    m = size(get_subblock(array, 1, 1), 1)
    A = first_hankel_block[1+m:end-X_rows, :]
    B = second_hankel_block[1:size(A, 1), :]
    C = second_hankel_block[size(A, 1)+1:end, :]

    pslrc = PartiallySpecifiedLRCompletion(A, B, C, tol = tol)
    check_lrcompletion(pslrc.lrc)

    # # For debugging
    # display(pslrc)

    for i = 2:num_blocks-2

        rows = size(get_subblock(array, i, 1), 1)
        if rows > 0
            pslrc = removal_of_rows(pslrc, rows, tol = tol)

            # # For debugging
            # println("After Removal of Rows")
            # display(pslrc)
        end

        cols = get_columnblock(array, i + 1)
        if size(cols, 2) > 0
            pslrc = addition_of_columns(pslrc, cols, tol = tol)

            # # For debugging
            # println("After Addition of Columns")
            # display(pslrc)
        end

        if size(pslrc.lrc.colint.QTAbar, 1) == 0
            break
        end

    end

    return particular_solution(pslrc)

end

function overlapping_hankel_lrc_triangle_onestage(array::TriangularArray;
    tol = DEFAULT_TOL)

    throw(ErrorException("This has not been completely implemented now"))

    num_blocks = array.num_blocks

    if (num_blocks == 1)
        return zeros(size(get_subblock(array, num_blocks, 1)))
    end

    X_size = size(get_subblock(array, num_blocks, 1))
    X_rows = X_size[1]

    first_hankel_block = get_columnblock(array, 1)
    second_hankel_block = get_columnblock(array, 2)

    if num_blocks >= 4
        # Eliminate to ensure R(B^T) ∩ R(F^T) = 0
        rows = size(get_subblock(array, 2, 1), 1)

        m = size(get_subblock(array, 1, 1), 1)
        E = first_hankel_block[1+m:m+rows, :]
        A = first_hankel_block[1+m+rows:end-X_rows, :]
        F = first_hankel_block[1:rows, :]
        B = second_hankel_block[1+rows:rows+size(A, 1), :]

        new_rows = eliminate_up(E, F, A, B, tol = tol)

        A = [new_rows[:, 1:size(A, 2)]; A]
        B = [new_rows[:, 1+size(A, 2):end]; A]
    else
        m = size(get_subblock(array, 1, 1), 1)
        A = first_hankel_block[1+m:end-X_rows, :]
        B = second_hankel_block[1:size(A, 1), :]
    end

    C = second_hankel_block[size(A, 1)+1:end, :]

    pslrc = PartiallySpecifiedLRCompletion(A, B, C, tol = tol)
    check_lrcompletion(pslrc.lrc)

    for i = 2:num_blocks-2

        rows = size(get_subblock(array, i, 1), 1)
        pslrc = removal_of_rows(pslrc, rows, tol = tol)

        # Do some elimination up
        cols = get_columnblock(array, i + 1)
        G = cols[1:size(pslrc.lrc.A, 1), :]
        H = cols[1+size(pslrc.lrc.A, 1):end, :]
        cols = eliminate_up(H', G', pslrc.lrc.C', pslrc.lrc.B', tol = tol)'

        if i < num_blocks - 2
            rows = size(get_subblock(array, i + 1, 1), 1)
            E = pslrc.lrc.A[1:rows, :]
            A = pslrc.lrc.A[1+rows:end, :]
            BG = [pslrc.lrc.B cols[1:size(G, 1), :]]
            F = BG[1:rows, :]
            B = BG[1+rows:end, :]
            new_rows = eliminate_up(E, F, A, B, tol = tol)
        end
        error()

        pslrc = addition_of_columns(pslrc, cols, tol = tol)

    end

    return particular_solution(pslrc)

end


end