module CSS

using LinearAlgebra, SparseArrays, ..OverlappingHankel, ..Utilities


export CSSmatrix, CSSsolve, CSStoDense, constructCSS, CSSmatrixvectormultiply


struct CSSmatrix
    N::Int64
    n::Vector{Int64}
    Gpi::Vector{Int64}   # hankel block ranks lower triangular part
    Hpi::Vector{Int64}   # hankel block ranks upper triangular part
    # main diagonal
    Di::Vector{Matrix}
    # upper triangular part
    Ui::Vector{Matrix}
    Wi::Vector{Matrix}
    Vi::Vector{Matrix}
    V0::Matrix
    # lower triangular part
    Pi::Vector{Matrix}
    Ri::Vector{Matrix}
    Qi::Vector{Matrix}
    P0::Matrix

    function CSSmatrix(N, n, Gpi, Hpi, Di, Ui, Wi, Vi, V0, Pi, Ri, Qi, P0)


        if N < 3
            error("N<3 not supported")
        end

        # check input dimensions
        if length(n) != N || length(Di) != N || length(Ui) != N - 1 ||
           length(Wi) != N - 2 || length(Vi) != N - 1 || length(Pi) != N - 1 ||
           length(Ri) != N - 2 || length(Qi) != N - 1 || length(Hpi) != N - 1 ||
           length(Gpi) != N - 1
            error("Input dimensions do not agree")
        end

        # check diagonal matrix dimensions
        for i = 1:N
            if size(Di[i], 1) != n[i] || size(Di[i], 2) != n[i]
                error("Diagonal matrices dimensions mismatch")
            end
        end

        # check upper triangular matrix dimensions
        for i = 1:N-1
            if size(Ui[i], 1) != n[i] || size(Ui[i], 2) != Hpi[i]
                error("Ui matrices dimensions mismatch")
            end
            if size(Vi[i], 1) != n[i+1] || size(Vi[i], 2) != Hpi[i]
                error("Vi matrices dimensions mismatch")
            end
        end
        for i = 1:N-2
            if size(Wi[i], 1) != Hpi[i] || size(Wi[i], 2) != Hpi[i+1]
                error("Wi matrices dimensions mismatch")
            end
        end
        if size(V0, 1) != n[N] || size(V0, 2) != Hpi[1]
            error("upper circular term dimensions mismatch")
        end

        # check lower triangular matrix dimensions
        for i = 1:N-1
            if size(Pi[i], 1) != n[i+1] || size(Pi[i], 2) != Gpi[i]
                error("Pi matrices dimensions mismatch")
            end
            if size(Qi[i], 1) != n[i] || size(Qi[i], 2) != Gpi[i]
                error("Qi matrices dimensions mismatch")
            end
        end
        for i = 1:N-2
            if size(Ri[i], 1) != Gpi[i+1] || size(Ri[i], 2) != Gpi[i]
                error("Ri translation operators dimensions mismatch")
            end
        end
        if size(P0, 1) != n[N] || size(P0, 2) != Gpi[1]
            error("lower circular term dimensions mismatch")
        end

        new(N, n, Gpi, Hpi, Di, Ui, Wi, Vi, V0, Pi, Ri, Qi, P0)


    end

end




function CSStoDense(A::CSSmatrix)
    # assertions

    ntot = sum(A.n)
    Adense = zeros(ntot, ntot)
    off = [0; cumsum(A.n)]

    N = A.N

    #fill up diagonal part
    for i = 1:A.N
        Adense[off[i]+1:off[i]+A.n[i], off[i]+1:off[i]+A.n[i]] = A.Di[i]
    end

    # fill up lower triangular part
    for j = 1:(A.N-1)
        temp = transpose(A.Qi[j])
        for i = j+1:A.N
            Adense[off[i]+1:off[i]+A.n[i], off[j]+1:off[j]+A.n[j]] = A.Pi[i-1] * temp
            if i != A.N
                temp = A.Ri[i-1] * temp
            end
        end
    end

    #add circular term
    Adense[off[N]+1:off[N]+A.n[N], off[1]+1:off[1]+A.n[1]] = Adense[off[N]+1:off[N]+A.n[N], off[1]+1:off[1]+A.n[1]] + A.P0 * transpose(A.Qi[1])

    # fill up upper triangular part
    for j = A.N:-1:2
        temp = transpose(A.Vi[j-1])
        for i = j-1:-1:1
            Adense[off[i]+1:off[i]+A.n[i], off[j]+1:off[j]+A.n[j]] = A.Ui[i] * temp
            if i != 1
                temp = A.Wi[i-1] * temp
            end
        end
    end

    #add circular term
    Adense[off[1]+1:off[1]+A.n[1], off[N]+1:off[N]+A.n[N]] = Adense[off[1]+1:off[1]+A.n[1], off[N]+1:off[N]+A.n[N]] + A.Ui[1] * transpose(A.V0)


    return Adense
end


function CSSmatrixvectormultiply(A::CSSmatrix, x::Vector)
    # assertions
    @assert sum(A.n) == length(x) "Matrix vector dimensions not consistent"

    # break up vector
    xi = partitionVector(x, A.n)

    N = A.N
    
    ### SSS multiply ###
    # Diagonal terms
    bi = [A.Di[i] * xi[i] for i = 1:N]
    # backward flow (upper triangular part)
    g = transpose(A.Vi[N-1]) * xi[N]
    bi[N-1] = bi[N-1] + A.Ui[N-1] * g
    for i = N-2:-1:1
        g = A.Wi[i] * g + transpose(A.Vi[i]) * xi[i+1]
        bi[i] = bi[i] + A.Ui[i] * g
    end
    # forward flow (lower triangular part)
    h = transpose(A.Qi[1]) * xi[1]
    bi[2] = bi[2] + A.Pi[1] * h
    for i = 2:1:N-1
        h = A.Ri[i-1] * h + transpose(A.Qi[i]) * xi[i]
        bi[i+1] = bi[i+1] + A.Pi[i] * h
    end

    # circular terms
    bi[N] = bi[N] + A.P0 * transpose(A.Qi[1]) * xi[1]
    bi[1] = bi[1] + A.Ui[1] * transpose(A.V0) * xi[N]


    #concat back into vector
    b = foldr(vcat, bi)

    return b
end


function constructCSS(A::Matrix, n::Vector{Int64}; threshold::Float64 = 1E-10)
    # assertions
    @assert size(A, 1) == size(A, 2) "Matrix is not square"
    @assert sum(n) == size(A, 1) "Matrix partition is not valid"



    # some definitions
    N = length(n)
    off::Vector{Int64} = [0; cumsum(n)]

    #### ------------------------------------ ####
    #### ----  Define diagonal matrices  ---- ####
    #### ------------------------------------ ####

    Di = Matrix{Float64}[]
    for i = 1:N
        push!(Di, A[off[i]+1:off[i]+n[i], off[i]+1:off[i]+n[i]])
    end

    #### ------------------------------------- ####
    #### - Solve Low rank completion problem - ####
    #### ------------------------------------- ####

    A_N1original = deepcopy(A[off[N]+1:off[N]+n[N], off[1]+1:off[1]+n[1]])
    A_1Noriginal = deepcopy(A[off[1]+1:off[1]+n[1], off[N]+1:off[N]+n[N]])

    A_N1modified, A_1Nmodified = overlapping_hankel_lrc(A, n, tol = threshold)
    display(A_N1modified)

    # display(A_N1modified)
    # display(A_1Nmodified)
    # A_N1modified = rand(n[N],n[1])
    # A_1Nmodified = rand(n[1],n[N])

    # modified matrix
    Acopy = deepcopy(A)
    Acopy[off[1]+1:off[1]+n[1], off[N]+1:off[N]+n[N]] = A_1Nmodified
    Acopy[off[N]+1:off[N]+n[N], off[1]+1:off[1]+n[1]] = A_N1modified

    #### ------------------------------------- ####
    #### ----  Define Lower hankel blocks ---- ####
    #### ------------------------------------- ####



    ### Apply classical SSS construction to modified matrix ###

    Gpi = Int64[]
    Pi = Matrix{Float64}[]
    Ri = Matrix{Float64}[]
    Qi = Matrix{Float64}[]

    # initialize
    Z = zeros(sum(n[2:end]), 0)
    Vn = zeros(0, 0)
    r = 0
    Rightprev = []
    for i = 1:N-1

        Z = hcat(Z, Acopy[off[i+1]+1:end, off[i]+1:off[i+1]])
        U, sigma, V, p = lowrankapprox(Z, threshold)
        Vn = [Vn * V[1:r, :]
            V[r+1:end, :]]
        r = p

        Right = Vn * Diagonal(sigma)


        # add Gpi
        push!(Gpi, p)
        # add Pi
        push!(Pi, U[1:n[i+1], :])
        # add Qi
        push!(Qi, Right[off[i]+1:end, :])
        if i != 1
            # add Ri
            push!(Ri, transpose(Rightprev \ Right[1:off[i], :]))
        end

        Z = U[n[i+1]+1:end, :] * Diagonal(sigma)
        Rightprev = deepcopy(Right)
    end

    ### Modify terms to obtain true matrix entries ###

    U, sigma, Q_delta, ~ = lowrankapprox(A_N1original - A_N1modified, threshold)
    P0delta = U * Diagonal(sigma)
    ~, ~, Q1_new, Gp1_new = lowrankapprox(transpose([Qi[1] Q_delta]), threshold)

    # the updates
    P0 = P0delta * transpose(Q_delta) * Q1_new
    Pi[1] = Pi[1] * transpose(Qi[1]) * Q1_new
    Ri[1] = Ri[1] * transpose(Qi[1]) * Q1_new
    Qi[1] = Q1_new
    Gpi[1] = Gp1_new


    #### ------------------------------------- ####
    #### ----  Define upper hankel blocks ---- ####
    #### ------------------------------------- ####


    ### Apply classical SSS construction to modified matrix ###


    Hpi = Int64[]
    Ui = Matrix{Float64}[]
    Wi = Matrix{Float64}[]
    Vi = Matrix{Float64}[]

    # initialize
    Z = zeros(0, sum(n[2:end]))
    Un = zeros(0, 0)
    r = 0
    Leftprev = []
    for i = 1:N-1
        Z = vcat(Z, Acopy[off[i]+1:off[i+1], off[i+1]+1:end])
        U, sigma, V, p = lowrankapprox(Z, threshold)
        Un = [Un * U[1:r, :]
            U[r+1:end, :]]
        r = p

        Left = Un * Diagonal(sigma)

        # add Hpi
        push!(Hpi, p)
        # add Ui
        push!(Ui, Left[off[i]+1:end, :])

        # add Vi
        push!(Vi, V[1:n[i+1], :])
        if i != 1
            # add Wi
            push!(Wi, Leftprev \ Left[1:off[i], :])
        end

        Z = Diagonal(sigma) * transpose(V[n[i+1]+1:end, :])
        Leftprev = deepcopy(Left)

    end


    ### Modify terms to obtain true matrix entries ###
    U_delta, sigma, R, ~ = lowrankapprox(A_1Noriginal - A_1Nmodified, threshold)
    V0delta = R * Diagonal(sigma)
    U1_new, ~, ~, Hp1_new = lowrankapprox([Ui[1] U_delta], threshold)

    # the updates
    V0 = V0delta * transpose(U_delta) * U1_new
    Vi[1] = Vi[1] * transpose(Ui[1]) * U1_new
    Wi[1] = transpose(U1_new) * Ui[1] * Wi[1]
    Ui[1] = U1_new
    Hpi[1] = Hp1_new

    #### ----------------------------------------- ####
    #### ----  Construct Circular SSS matrix  ---- ####
    #### ----------------------------------------- ####

    A_circSSS = CSSmatrix(N, n, Gpi, Hpi, Di, Ui, Wi, Vi, V0, Pi, Ri, Qi, P0)

    return A_circSSS
end


function CSSsolve(A::CSSmatrix, b::Vector)


    N_sparse = sum(A.Gpi) + sum(A.Hpi) + sum(A.n) + size(A.P0, 2) + size(A.Ui[1], 2)



    Blocksizes = Int64[]
    vecsizes = Array{Int64}(undef, A.N, 3)
    for i = 1:A.N
        if i == 1
            vecsizes[i, :] = [A.n[i] size(A.P0, 2) A.Gpi[i]]
            append!(Blocksizes, A.n[i] + A.Gpi[i] + size(A.P0, 2))
        elseif i == A.N
            vecsizes[i, :] = [A.n[i] A.Hpi[i-1] size(A.Ui[1], 2)]
            append!(Blocksizes, A.n[i] + A.Hpi[i-1] + size(A.Ui[1], 2))
        else
            vecsizes[i, :] = [A.n[i] A.Hpi[i-1] A.Gpi[i]]
            append!(Blocksizes, A.n[i] + A.Gpi[i] + A.Hpi[i-1])
        end
    end

    Indices = pushfirst!(cumsum(Blocksizes), 0)
    pop!(Indices)

    #### ----------------------------------- ####
    #### ----  construct sparse vector  ---- ####
    #### ----------------------------------- ####


    I = Int64[]
    for i = 1:A.N
        append!(I, collect((Indices[i]+1):(Indices[i]+A.n[i])))
    end

    b_sparse = sparsevec(I, b, N_sparse)


    #### ----------------------------------- ####
    #### ----  construct sparse matrix  ---- ####
    #### ----------------------------------- ####

    I = Int64[]
    J = Int64[]
    V = Float64[]
    # add the A's
    for i = 1:A.N

        if i == 1
            Ak = [A.Di[i] zeros(vecsizes[i, 1], vecsizes[i, 2]) zeros(vecsizes[i, 1], vecsizes[i, 3])
                transpose(A.Qi[i]) -eye(vecsizes[i, 2]) zeros(vecsizes[i, 2], vecsizes[i, 3])
                transpose(A.Qi[i]) zeros(vecsizes[i, 3], vecsizes[i, 2]) -eye(vecsizes[i, 3])]
            Itemp, Jtemp, Vtemp = findnz(sparse(Ak))
        elseif i == A.N
            Ak = [A.Di[A.N] zeros(vecsizes[i, 1], vecsizes[i, 2]) zeros(vecsizes[i, 1], vecsizes[i, 3])
                transpose(A.Vi[A.N-1]) -eye(vecsizes[i, 2]) zeros(vecsizes[i, 2], vecsizes[i, 3])
                transpose(A.V0) zeros(vecsizes[i, 3], vecsizes[i, 2]) -eye(vecsizes[i, 3])]
            Itemp, Jtemp, Vtemp = findnz(sparse(Ak))
        else
            Ak = [A.Di[i] zeros(vecsizes[i, 1], vecsizes[i, 2]) zeros(vecsizes[i, 1], vecsizes[i, 3])
                transpose(A.Vi[i-1]) -eye(vecsizes[i, 2]) zeros(vecsizes[i, 2], vecsizes[i, 3])
                transpose(A.Qi[i]) zeros(vecsizes[i, 3], vecsizes[i, 2]) -eye(vecsizes[i, 3])]
            Itemp, Jtemp, Vtemp = findnz(sparse(Ak))
        end

        append!(I, Indices[i] .+ Itemp)
        append!(J, Indices[i] .+ Jtemp)
        append!(V, Vtemp)


    end


    # add the B's
    for i = 2:A.N
        if i == A.N
            Bk = [zeros(vecsizes[i, 1], vecsizes[i-1, 1]) zeros(vecsizes[i, 1], vecsizes[i-1, 2]) A.Pi[i-1]
                zeros(vecsizes[i, 2], vecsizes[i-1, 1]) zeros(vecsizes[i, 2], vecsizes[i-1, 2]) zeros(vecsizes[i, 2], vecsizes[i-1, 3])
                zeros(vecsizes[i, 3], vecsizes[i-1, 1]) zeros(vecsizes[i, 3], vecsizes[i-1, 2]) zeros(vecsizes[i, 3], vecsizes[i-1, 3])]
            Itemp, Jtemp, Vtemp = findnz(sparse(Bk))
        else
            Bk = [zeros(vecsizes[i, 1], vecsizes[i-1, 1]) zeros(vecsizes[i, 1], vecsizes[i-1, 2]) A.Pi[i-1]
                zeros(vecsizes[i, 2], vecsizes[i-1, 1]) zeros(vecsizes[i, 2], vecsizes[i-1, 2]) zeros(vecsizes[i, 2], vecsizes[i-1, 3])
                zeros(vecsizes[i, 3], vecsizes[i-1, 1]) zeros(vecsizes[i, 3], vecsizes[i-1, 2]) A.Ri[i-1]]
            Itemp, Jtemp, Vtemp = findnz(sparse(Bk))
        end

        append!(I, Indices[i] .+ Itemp)
        append!(J, Indices[i-1] .+ Jtemp)
        append!(V, Vtemp)


    end

    # add the C's
    for i = 2:A.N
        if i == 2
            Ck = [zeros(vecsizes[i-1, 1], vecsizes[i, 1]) A.Ui[i-1] zeros(vecsizes[i-1, 1], vecsizes[i, 3])
                zeros(vecsizes[i-1, 2], vecsizes[i, 1]) zeros(vecsizes[i-1, 2], vecsizes[i, 2]) zeros(vecsizes[i-1, 2], vecsizes[i, 3])
                zeros(vecsizes[i-1, 3], vecsizes[i, 1]) zeros(vecsizes[i-1, 3], vecsizes[i, 2]) zeros(vecsizes[i-1, 3], vecsizes[i, 3])]
            Itemp, Jtemp, Vtemp = findnz(sparse(Ck))
        else
            Ck = [zeros(vecsizes[i-1, 1], vecsizes[i, 1]) A.Ui[i-1] zeros(vecsizes[i-1, 1], vecsizes[i, 3])
                zeros(vecsizes[i-1, 2], vecsizes[i, 1]) A.Wi[i-2] zeros(vecsizes[i-1, 2], vecsizes[i, 3])
                zeros(vecsizes[i-1, 3], vecsizes[i, 1]) zeros(vecsizes[i-1, 3], vecsizes[i, 2]) zeros(vecsizes[i-1, 3], vecsizes[i, 3])]
            Itemp, Jtemp, Vtemp = findnz(sparse(Ck))
        end

        append!(I, Indices[i-1] .+ Itemp)
        append!(J, Indices[i] .+ Jtemp)
        append!(V, Vtemp)


    end

    # add E
    E = [zeros(vecsizes[1, 1], vecsizes[A.N, 1]) zeros(vecsizes[1, 1], vecsizes[A.N, 2]) A.Ui[1]
        zeros(vecsizes[1, 2], vecsizes[A.N, 1]) zeros(vecsizes[1, 2], vecsizes[A.N, 2]) zeros(vecsizes[1, 2], vecsizes[A.N, 3])
        zeros(vecsizes[1, 3], vecsizes[A.N, 1]) zeros(vecsizes[1, 3], vecsizes[A.N, 2]) zeros(vecsizes[1, 3], vecsizes[A.N, 3])]
    Itemp, Jtemp, Vtemp = findnz(sparse(E))

    append!(I, Indices[1] .+ Itemp)
    append!(J, Indices[A.N] .+ Jtemp)
    append!(V, Vtemp)

    # add D
    D = [zeros(vecsizes[A.N, 1], vecsizes[1, 1]) A.P0 zeros(vecsizes[A.N, 1], vecsizes[1, 3])
        zeros(vecsizes[A.N, 2], vecsizes[1, 1]) zeros(vecsizes[A.N, 2], vecsizes[1, 2]) zeros(vecsizes[A.N, 2], vecsizes[1, 3])
        zeros(vecsizes[A.N, 3], vecsizes[1, 1]) zeros(vecsizes[A.N, 3], vecsizes[1, 2]) zeros(vecsizes[A.N, 3], vecsizes[1, 3])]
    Itemp, Jtemp, Vtemp = findnz(sparse(D))

    append!(I, Indices[A.N] .+ Itemp)
    append!(J, Indices[1] .+ Jtemp)
    append!(V, Vtemp)

    A_sparse = sparse(I, J, V, N_sparse, N_sparse)

    #### ------------------------------- ####
    #### ----  solve sparse system  ---- ####
    #### ------------------------------- ####

    x_sparse = qr(A_sparse) \ Vector(b_sparse)

    x = Float64[]
    for i = 1:A.N
        append!(x, x_sparse[(Indices[i]+1):(Indices[i]+A.n[i])])
    end

    return x
end





end