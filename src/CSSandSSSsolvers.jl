module CSSandSSSsolvers

export greet 
greet() = print("Hello! welcome to CSS and SSS solvers package")


include("utilities.jl")
include("lrc/overlapping_hankel.jl")
include("SSS.jl")
include("CSS.jl")

include("casestudies.jl")

# To be exported at top level

using .OverlappingHankel
export overlapping_hankel_lrc

using  .SSS
export SSSmatrix, SSSsolve, SSStoDense, constructSSS, SSSmatrixvectormultiply

using  .CSS
export CSSmatrix, CSSsolve, CSStoDense, constructCSS, CSSmatrixvectormultiply





end # module
